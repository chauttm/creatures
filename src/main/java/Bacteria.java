import java.awt.*;

/**
 * Created by dse on 12/14/2014.
 */
public class Bacteria extends Creature {
    private static final int MAX_AGE = 5;
    private static final int COLOR_STEP = 256/MAX_AGE;

    Bacteria(Board board, int _x, int _y) {
        super(board, _x, _y);
    }

    @Override
    protected Color getColor() {
        return new Color(0, COLOR_STEP * age, 0);
    }

    @Override
    protected int getMaxAge() {
        return MAX_AGE;  //To change body of implemented methods use File | Settings | File Templates.
    }


}
